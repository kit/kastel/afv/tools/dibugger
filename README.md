# DIbugger - Understanding Counterexamples for Relational Program Properties

DIbugger is a tool that allows to debug through multiple programs simultaneously and evaluate relationships between those executions.
A detailed explanation can be found in the paper [Herda et al. 2019](https://formal.kastel.kit.edu/biblio/?lang=en&key=HerdaKirstenEA2019).

## Getting Started

You can run DIbugger via the following command:

```
java -jar dibugger.jar
```


## Contributors

This software was designed and implemented during the bachelor course [Software Engineering Practice](https://formal.kastel.kit.edu/teaching/pse/201718/) by:

* Etienne Brunner
* Joana Plewnia
* Ulla Scheler
* Chiara Staudenmaier
* Benedikt Wagner
* Pascal Zwick

For more information, please contact [Mihai Herda](https://formal.kastel.kit.edu/~herda/?lang=en) or [Michael Kirsten](https://formal.kastel.kit.edu/~kirsten/?lang=en).
